#!/usr/bin/env bash


#=======================#
#                       #
#### UPDATE PKGBUILD ####
#                       #
#=======================#


### Environmental Vars ###
DEBARCH=armhf
VIVALDI_STREAM=vivaldi-snapshot


### Required Programmes ###
if available wget; then
    SILENT_DL="wget -qO-"
elif available curl; then
    SILENT_DL="curl -fs"
else
    echo 'curl or wget must be installed to use this tool.'
    exit 1
fi


### Package Version ###
PKGVER_OLD=$(grep '^pkgver=' PKGBUILD | sed 's/^pkgver=//')
PKGREL_OLD=$(grep '^pkgrel=' PKGBUILD | sed 's/^pkgrel=//')


### Server Version ###
VER_REL_NEW=$SILENT_DL "https://repo.vivaldi.com/archive/deb/dists/stable/main/binary-$DEBARCH/Packages.gz" | \
gzip -d | \
grep -A6 -x "Package: $VIVALDI_STREAM" | \
sed -n 's/^Version: \(\([0-9]\+\.\)\{3\}[0-9]\+-[0-9]\+\)/\1/p' | \
sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | \
tail -n 1

PKGVER_NEW=${VER_REL_NEW/-*/}
PKGREL_NEW=${VER_REL_NEW/*-/}

UPD=0
if [ $PKGVER_OLD != $PKGVER_NEW ]; then

    sed -i "s/^pkgver=.*/pkgver=${PKGVER_NEW}/"
    UPD=1

fi
if [ $PKGREL_OLD < $PKGREL_NEW ]; then

    sed -i "s/^pkgrel=.*/pkgrel=${PKGVER_NEW}/"
    UPD=1

fi
if [ $UPD == 1 ]; then

    updpkgsums

fi
