# Maintainer: Feakster <feakster at posteo dot eu>
# Contributor: BlackIkeEagle <ike dot devolder at gmail dot com>
# Contributor: Lukas Kucharczyk <lukas at kucharczyk dot xyz>
# Contributor: Felix Golatofski <contact at xdfr dot de>
# Contributor: Matthew Zilvar <mattzilvar at gmail dot com>
# Contributor: Térence Clastres <t dot clastres at gmail dot com>

### Notes ###
# - Linux snapshot installer script (https://downloads.vivaldi.com/snapshot/install-vivaldi.sh).
# - Vivaldi appear to have dropped support for i386 builds, which are still at version 3.7.2218.58-1.

### Info ###
pkgname=vivaldi
_debver=7.1.3570.54-1
pkgver=${_debver/-*/}
pkgrel=1
pkgdesc='An advanced browser made with the power user in mind.'
arch=('armv7h' 'aarch64' 'x86_64') # 'i686' 'pentium4'
url='https://vivaldi.com'
license=('custom:Vivaldi EULA')
provides=('www-browser')
conflicts=('vivaldi-codecs-ffmpeg-extra-bin' 'vivaldi-ffmpeg-codecs' 'vivaldi-update-ffmpeg-hook' 'vivaldi-widevine')
replaces=('vivaldi-arm-bin')
depends=('alsa-lib' 'desktop-file-utils' 'gtk3' 'hicolor-icon-theme' 'libcups' 'libxss' 'nss' 'shared-mime-info' 'ttf-font')
optdepends=(
    'libnotify: native notifications'
)
options=('!emptydirs' '!strip')
source=('remove-ffmpeg-widevine.hook' 'update-ffmpeg.hook' 'update-widevine.hook')
source_armv7h=("https://downloads.$pkgname.com/stable/$pkgname-stable_${_debver}_armhf.deb")
source_aarch64=("https://downloads.$pkgname.com/stable/$pkgname-stable_${_debver}_arm64.deb")
source_x86_64=("https://downloads.$pkgname.com/stable/$pkgname-stable_${_debver}_amd64.deb")
b2sums=('6c99964b9a461f4870d0ebff6b7e5085bd23c37213748e68380ff0eb067c2dd08ebcb5ff27ba83209746643349c17c17fdf73bf71557059f829ecf8ee636562f'
        '52e13a59911fd8863b3e5f7f0f08e75b7b5c49012435efd01bf01a7ce66f65d768153500312ab2f41b7a3723b78fba0559af6370ff52f4fb4605c97e85fd0e37'
        '2fda088a7ad7df8d756231da9e2a3edd59112f1da87f9b885127afafc1f89b20a68c8681c4c7749a10521269862f6e81493798c7b80a2213626642581558a31b')
b2sums_armv7h=('9545176d0f3ff828814ea2dc0e01b0e1e280badd1e06e524743072069dc88fe6978e67684fbb544d799815e5d8b9f3ab7cd0e7739ad5f5bae02b328ea2374170')
b2sums_aarch64=('b6d4235ce67975bde8ae878819f6c08c9d5ce61b0ad3a6dafa0912d43c95bf8ac5a1d1e3daf7a5f5ace5843fab8f7ca2142af3fa03399a66319fdc4346bd4f1f')
b2sums_x86_64=('ce5e50d317d5fffb56487f19793dcebc06a583e5cf74a5ef5a2b28a81ada0407f6b04eb8c1983323dacddf8b6c376d5f7658311691bfda9a24e47d863ed04936')

### Prepare ###
prepare() {

    ## Extract Internals ##
    tar -xf "$srcdir"/data.tar.xz -C "$srcdir"
    rm "$srcdir"/{_gpgbuilder,debian-binary,control.tar.xz,data.tar.xz}

    ## Remove Debian-Related Directories/Files ##
    rm -rf "$srcdir"/opt/$pkgname/cron

    ## Fix File Permissions ##
    find $srcdir -type f -regex '^.+\.so\(\.[0-9]+\)?$' -exec chmod 0644 {} \;

}

### Package ###
## All ##
package() {

    ## Change Directory ##
    cd "$srcdir"

    ## Copy Directory Structure ##
    cp --parents -a {opt,usr} "$pkgdir"

    ## SUID Sandbox ##
    chmod 4755 "$pkgdir"/opt/$pkgname/$pkgname-sandbox

    ## Executable ##
    ln -fs \
        /opt/$pkgname/$pkgname \
        "$pkgdir"/usr/bin/$pkgname-stable

    ## Icons ##
    for res in 16 22 24 32 48 64 128 256; do
        install -d "$pkgdir"/usr/share/icons/hicolor/${res}x$res/apps
        ln -fs \
            /opt/$pkgname/product_logo_$res.png \
            "$pkgdir"/usr/share/icons/hicolor/${res}x$res/apps/$pkgname.png
    done

    ## Pacman Hooks ##
    install -Dm0644 \
        "$srcdir"/update-ffmpeg.hook \
        "$pkgdir"/usr/share/libalpm/hooks/$pkgname-update-ffmpeg.hook
    install -Dm0644 \
        "$srcdir"/remove-ffmpeg-widevine.hook \
        "$pkgdir"/usr/share/libalpm/hooks/$pkgname-remove-ffmpeg-widevine.hook

    ## License ##
    install -d "$pkgdir"/usr/share/licenses/$pkgname
    ln -fs \
        /opt/$pkgname/LICENSE.html \
        "$pkgdir"/usr/share/licenses/$pkgname/LICENSE.html

}

## x86_64 ##
package_x86_64() {

    ## Pacman Hooks ##
    install -Dm0644 \
        "$srcdir"/update-widevine.hook \
        "$pkgdir"/usr/share/libalpm/hooks/$pkgname-update-widevine.hook

}

## armv7h ##
package_armv7h() {

    ## Pacman Hooks ##
    install -Dm0644 \
        "$srcdir"/update-widevine.hook \
        "$pkgdir"/usr/share/libalpm/hooks/$pkgname-update-widevine.hook

}
